﻿create database DBZStoreDB;

use DBZStoreDB;


create table tb_produto (
	id_produto int primary key auto_increment,
    nm_produto varchar(100),
    vl_preco decimal(15,2)
);

select * from tb_produto;

